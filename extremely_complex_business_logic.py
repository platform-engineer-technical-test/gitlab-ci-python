from authentication import AuthenticationService, AuthenticationResult

def authenticateCustomer(username: str, password: str):
  authentication_service = AuthenticationService()
  result = authentication_service.login(username, password)
  if result == AuthenticationResult.SUCCESS:
    print("Authentication successful!")
  else:
    raise "Auth failed!"
