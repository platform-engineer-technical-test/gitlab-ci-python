from enum import Enum

class AuthenticationResult(Enum):
  SUCCESS = 0
  FAILURE = 1

class AuthenticationService:
  def login(self, username: str, password: str):
    # do business logic here
    return AuthenticationResult.SUCCESS
