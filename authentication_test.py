import unittest

from authentication import AuthenticationService, AuthenticationResult

USERNAME = 'user'
PASSWORD = 'pass'

class TestAuthenticationService(unittest.TestCase):
  def test_given_any_credentials_when_login_then_return_success_result(self):
    # given
    authentication_service = AuthenticationService()

    # when
    result = authentication_service.login(USERNAME, PASSWORD)

    # then
    self.assertEqual(result, AuthenticationResult.SUCCESS)
