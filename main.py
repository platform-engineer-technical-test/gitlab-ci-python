from extremely_complex_business_logic import authenticateCustomer

def main():
  authenticateCustomer("user", "pass")

if __name__ == '__main__':
  main()
