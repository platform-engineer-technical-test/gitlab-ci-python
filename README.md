# gitlab-ci-python

Dummy project to test your GitLab-CI troubleshooting skills with a Python project!

## Requirements

* Python 3.10
* A working *nix terminal

## Setup

### Running the code locally

```bash
python3 -m venv ./venv
source ./venv/bin/activate
python main.py
```

### Running unit tests locally

```bash
python -m unittest discover . "*_test.py" --verbose
```
